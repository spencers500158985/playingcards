
// Playing Cards
// Spencer Sauberlich

#include <iostream>
#include <conio.h>

using namespace std;


enum Rank
{ 
	Two = 2, 
	Three, 
	Four, 
	Five, 
	Six, 
	Seven, 
	Eight, 
	Nine, 
	Ten, 
	Jack, 
	Queen, 
	King, 
	Ace
};
enum Suit
{
	Spade,
	Club,
	Heart,
	Diamond
};

struct Card
{
	Rank rank;
	Suit suite;
};


int main()
{


	_getch();
	return 0;
}
